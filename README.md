# gitlab-qa-executor

GitLab QA Executor is CI configuration to run GitLab QA against self-managed GitLab environments with parallelization in CI.
Configuration follows documentation in [Running GitLab QA](https://docs.gitlab.com/charts/development/gitlab-qa/).

1. Trigger a [new pipeline](https://gitlab.com/gitlab-org/quality/gitlab-qa-executor/-/pipelines/new) with the below variables:
   - `TEST_SUITE_SELECTION` - *required* - The test suite to run:
      | Job name          | Description         | Options used                 |
      |-------------------|---------------------|------------|
      | `Smoke`          | Small [subset of fast end-to-end functional tests](https://docs.gitlab.com/ee/development/testing_guide/smoke.html) to quickly ensure that basic functionality is working | `'--tag smoke'`                                                                            |
      | `Full`           | Running all tests against the environment. Test run will take more than an hour.                                                                                          | `'--tag ~skip_live_env --tag ~orchestrated --tag ~transient'`                              |
      | `Full without Smoke` | Running all tests against the environment except for smoke. Helpful if you want to run Full after Smoke suite.                                                                | `'--tag ~smoke  --tag ~skip_live_env --tag ~orchestrated --tag ~transient'` |
      | `Custom` | Running custom subset of tests or specific test against the environment by adding `TEST_SUITE` environment variable to the job. For example, passing `TEST_SUITE=--tag transient --tag ~skip_live_env --tag ~orchestrated` will run only transient tests.          | Custom options specified with `TEST_SUITE` |

   - `ROOT_PASSWORD` - *required* - This will be the password for the `root` user.
   - `QA_ENVIRONMENT_URL` - *required* - The URL to the target GitLab instance.
   - `QA_IMAGE` - *preferable* - QA image to use against the environment. If no `QA_IMAGE` provided, jobs will use `gitlab/gitlab-ee-qa:nightly` by default, but it's preferred to provide the specific revision to ensure QA image corresponds with GitLab instance version. To do that, check version and revision value for the target GitLab instance by [navigating to a Help page](https://docs.gitlab.com/ee/user/version.html) or running [`sudo gitlab-rake gitlab:env:info`](https://docs.gitlab.com/ee/administration/raketasks/maintenance.html#gather-gitlab-and-system-information):
      - If environment is on `pre` images, fetch the revision number of the GitLab environment, check if corresponding QA image exists in GitLab container repository [gitlab-ee-qa](https://gitlab.com/gitlab-org/gitlab/container_registry/3728789). If it exists, paste the value in `registry.gitlab.com/gitlab-org/gitlab/gitlab-ee-qa:{revision}`.
      - If environment is on stable release - search for the version at [gitlab/gitlab-ee-qa](https://hub.docker.com/r/gitlab/gitlab-ee-qa/tags) and specify the tag accordingly. For example, `gitlab/gitlab-ee-qa:16.3.2-ee`.
   - `EE_LICENSE` - optional - A string containing a GitLab EE license. This can be handled via `export EE_LICENSE=$(cat GitLab.gitlab-license)`. Can be omitted if environment already has a license.
   - `GITLAB_USERNAME` - optional - default is `root`.
   - `GITLAB_PASSWORD` - optional - default is `$ROOT_PASSWORD`.
   - `GITLAB_ADMIN_USERNAME` - optional - default is `root`.
   - `GITLAB_ADMIN_PASSWORD` - optional - default is `$ROOT_PASSWORD`.
   - `RELEASE` - optional - default is `EE`. If running against CE, update the value and QA image accordingly.

2. The selected suite will run automatically and you can trigger other suites manually if desired.

## Recorded tutorial

Live recording of a hands-on walk through of how to use this project: https://youtu.be/d72yvJhGYSE?feature=shared
